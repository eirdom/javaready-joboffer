package com.junioroffers.config;

import java.time.Duration;

import com.junioroffers.infrastracture.RemoteOfferClient;
import com.junioroffers.infrastracture.offer.client.OfferHttpClient;
import com.junioroffers.infrastracture.offer.error.RestTemplateResponseErrorHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class Config {

    @Bean
    RestTemplateResponseErrorHandler restTemplateResponseErrorHandler() {
        return new RestTemplateResponseErrorHandler();
    }

    @Bean
    RestTemplate restTemplate(@Value("${offer.http.client.config.connectionTimeout}") long connectionTimeout,
                              @Value("${offer.http.client.config.readTimeout}") long readTimeout,
                              RestTemplateResponseErrorHandler restTemplateResponseErrorHandler) {
        return new RestTemplateBuilder()
                .errorHandler(restTemplateResponseErrorHandler)
                .setConnectTimeout(Duration.ofMillis(connectionTimeout))
                .setReadTimeout(Duration.ofMillis(readTimeout))
                .build();
    }

    @Bean
    RemoteOfferClient remoteOfferClient(RestTemplate restTemplate, @Value("${offer.http.client.config.uri:http://example.com}") String uri) {
        return new OfferHttpClient(restTemplate, uri);
    }
}
