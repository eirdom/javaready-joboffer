package com.junioroffers.offer.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Objects;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
@Document
public class Offer {
    @Id
    private String id;
    private String position;
    private String companyName;
    private String salary;
    private String offerUrl;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Offer offer = (Offer) o;
        return Objects.equals(id, offer.id) &&
                Objects.equals(position, offer.position) &&
                Objects.equals(companyName, offer.companyName) &&
                Objects.equals(salary, offer.salary) &&
                Objects.equals(offerUrl, offer.offerUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, position, companyName, salary, offerUrl);
    }
}
