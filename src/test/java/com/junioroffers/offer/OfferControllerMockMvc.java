package com.junioroffers.offer;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class OfferControllerMockMvc implements SampleOfferDto{

    final String NO_EXISTING_ID = "100";

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;


    @Test
    public void should_return_two_expected_offers() throws Exception {
        List<OfferDto> sample = Arrays.asList(cybersourceOfferDto(),cdqPolandOfferDto());

//        final List<OfferDto> expectedOfferDtos = s.findAllOffers();
        String expectedResponseBody = objectMapper.writeValueAsString(sample);

        MvcResult mvcResult = mockMvc.perform(get("/offers"))
//                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(expectedResponseBody);
    }

    @Test
    public void should_return_offer_with_provided_id() throws Exception {
        final String exceptedResponseBody = objectMapper.writeValueAsString(cybersourceOfferDto());

        MvcResult mvcResult = mockMvc.perform(get("/offers/7b3e02b3-6b1a-4e75-bdad-cef5b279b074")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();

        String actualResponseBody = mvcResult.getResponse().getContentAsString();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(exceptedResponseBody);
    }

    @Test
    public void should_return_not_found_offer_with_provided_id() throws Exception {
        final String exceptedResponseBody = "Offer with id 100 is not found";

        MvcResult mvcResult = mockMvc.perform(get("/offers/" + NO_EXISTING_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andDo(print())
                .andReturn();

        String actualResponseBody = mvcResult.getResolvedException().getMessage();
        assertThat(actualResponseBody).isEqualToIgnoringWhitespace(exceptedResponseBody);
    }
}
