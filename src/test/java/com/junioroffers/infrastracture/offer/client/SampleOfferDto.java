package com.junioroffers.infrastracture.offer.client;

import com.junioroffers.infrastracture.offer.dto.OfferDto;

public interface SampleOfferDto {

    default OfferDto emptyOffer(){
        return new OfferDto();
    }

    default OfferDto offerWithParameters(String title, String company, String salary, String url){
        return OfferDto.builder().
                title(title).
                company(company).
                salary(salary).
                url(url).
                build();
    }
}
