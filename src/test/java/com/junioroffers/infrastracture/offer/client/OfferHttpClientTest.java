package com.junioroffers.infrastracture.offer.client;

import com.junioroffers.infrastracture.RemoteOfferClient;
import com.junioroffers.infrastracture.offer.dto.OfferDto;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

class OfferHttpClientTest implements SampleRestTemplateExchangeResponse, SampleOfferResponse, SampleOfferDto {

    @Test
    public void should_return_one_element_list_off_offers() {
        //given
        final RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ResponseEntity<List<OfferDto>> responseEntity = responseWithOneOffer();
        String url = "test";

        when(getExchange(restTemplate)).
                thenReturn(responseEntity);

        RemoteOfferClient offerHttpClient = new OfferHttpClient(restTemplate, url);
        //when
        List<OfferDto> offers = offerHttpClient.getOffers();
        //then
        assertThat(offers.size()).isEqualTo(1);
    }

    @Test
    public void should_return_empty_list_of_offers() {
        //given
        final RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ResponseEntity<List<OfferDto>> responseEntity = responseWithNoOffer();
        String url = "test";

        when(getExchange(restTemplate)).thenReturn(responseEntity);
        RemoteOfferClient offerHttpClient = new OfferHttpClient(restTemplate, url);
        //when
        List<OfferDto> offers = offerHttpClient.getOffers();

        //then
        assertThat(offers.size()).isEqualTo(0);
    }

    @Test
    public void should_return_two_offers() {
        //given
        final RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
        ResponseEntity<List<OfferDto>> responseEntity = responseWithTwoOffer(emptyOffer(), emptyOffer());
        String url = "test";

        when(getExchange(restTemplate)).
                thenReturn(responseEntity);
        RemoteOfferClient offerHttpClient = new OfferHttpClient(restTemplate, url);

        //when
        List<OfferDto> offers = offerHttpClient.getOffers();

        //then
        assertThat(offers.size()).isEqualTo(2);
    }
}