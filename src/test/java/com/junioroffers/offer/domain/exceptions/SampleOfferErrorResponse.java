package com.junioroffers.offer.domain.exceptions;

import org.springframework.http.HttpStatus;

interface SampleOfferErrorResponse {

    default OfferErrorResponse sampleOfferErrorResponse(){
        return new OfferErrorResponse("Offer with id 100 is not found", HttpStatus.NOT_FOUND);
    }




}