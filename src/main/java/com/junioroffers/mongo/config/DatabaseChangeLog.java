package com.junioroffers.mongo.config;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.junioroffers.offer.domain.Offer;
import com.junioroffers.offer.domain.OfferRepository;

import java.util.Arrays;
import java.util.List;

//@ChangeLog
@ChangeLog(order = "1")
public class DatabaseChangeLog {

    @ChangeSet(order = "001", id = "seedDatabase", author = "mateusz.swiniarski")
    public void seedDatabase(OfferRepository offerRepository){
        System.out.println(cdqPoland().getPosition());
        System.out.println("---------------");
        System.out.println(cyberSource().toString());
        List<Offer> offerList = Arrays.asList(cyberSource(), cdqPoland());
        offerRepository.insert(offerList);
    }

    private Offer cdqPoland(){
        return createOffer("24ee32b6-6b15-11eb-9439-0242ac13000",
                "Junior DevOps Engineer",
                "CDQ Poland",
                "8k - 14k PLN",
                "https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd");
    }

    private Offer cyberSource(){
        return createOffer( "7b3e02b3-6b1a-4e75-bdad-cef5b279b074",
                "Software Engineer - Mobile (m/f/d)",
                "Cybersource",
                "4k - 8k PLN",
                "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn");
    }

    private Offer createOffer(String id, String position, String companyName, String salary, String offerUrl){
        Offer offer = new Offer();
        offer.setId(id);
        offer.setPosition(position);
        offer.setCompanyName(companyName);
        offer.setSalary(salary);
        offer.setOfferUrl(offerUrl);
        return offer;
    }
}