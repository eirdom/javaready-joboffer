package com.junioroffers.offer.domain.dto;

import lombok.*;

@Getter
@Builder
@EqualsAndHashCode
public class OfferDto {

    private final String id;
    private final String position;
    private final String companyName;
    private final String salary;
    private final String offerURL;
}
