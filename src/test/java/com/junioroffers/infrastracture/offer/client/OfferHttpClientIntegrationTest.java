package com.junioroffers.infrastracture.offer.client;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.junioroffers.infrastracture.RemoteOfferClient;
import com.junioroffers.config.OfferHttpClientTestConfig;
import com.junioroffers.infrastracture.offer.dto.OfferDto;
import org.assertj.core.api.BDDAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.util.SocketUtils;
import wiremock.org.apache.http.HttpStatus;


import java.util.Arrays;
import java.util.Collections;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.assertj.core.api.BDDAssertions.then;


class OfferHttpClientIntegrationTest implements SampleOfferDto {

    int port = SocketUtils.findAvailableTcpPort();

    WireMockServer wireMockServer;

    RemoteOfferClient remoteOfferClient = new OfferHttpClientTestConfig().remoteOfferTestClient("http://localhost:" + port + "/offers", 1000, 1000);

    @BeforeEach
    void setup() {
        wireMockServer = new WireMockServer(options().port(port));
        wireMockServer.start();
        WireMock.configureFor(port);
    }

    @AfterEach
    void tearDown() {
        wireMockServer.stop();
    }


    @Test
    void should_return_two_job_offers() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_OK)
                        .withHeader("Content-Type", "application/json")
                        .withBody(bodyWithTwoOffersJson())));

        //when
        //then
        then(remoteOfferClient.getOffers())
                .containsExactlyInAnyOrderElementsOf(Arrays.asList(cybersourceSoftwareEngineer(), cdqJuniorDevOpsEngineer()));
    }

    @Test
    void should_return_one_job_offer() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                .withStatus(HttpStatus.SC_OK)
                .withHeader("Content-Type", "application/json")
                .withBody(bodyWithOneOfferJson())));
        //when
        //then
        then(remoteOfferClient.getOffers())
                .containsExactlyInAnyOrderElementsOf(Collections.singletonList(cybersourceSoftwareEngineer()));
    }

    @Test
    void should_return_zero_job_offers_when_status_is_no_content() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
            .willReturn(WireMock.aResponse()
            .withStatus(HttpStatus.SC_NO_CONTENT)
            .withHeader("Content-Type", "application/json")
            .withBody(bodyWithZeroOfferJson())));

        //when
        //then
        then(remoteOfferClient.getOffers())
                .isEmpty();
//                .size().isEqualTo(0);

    }

    @Test
    void should_return_zero_job_offers_when_status_is_ok() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
            .willReturn(WireMock.aResponse()
            .withStatus(HttpStatus.SC_OK)
            .withHeader("Content-Type", "application/json")
            .withBody(bodyWithZeroOfferJson())));

        //when
        //then
        then(remoteOfferClient.getOffers())
                .size().isEqualTo(0);
    }

    @Test
    void should_fail_with_connection_reset_by_peer() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                .withStatus(HttpStatus.SC_OK)
                .withHeader("Content-Type", "application/json")
                .withFault(Fault.CONNECTION_RESET_BY_PEER)));

        //when
        //then
        then(remoteOfferClient.getOffers())
                .size().isEqualTo(0);
    }

    @Test
    void should_fail_with_empty_response() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
            .willReturn(WireMock.aResponse()
            .withStatus(HttpStatus.SC_OK)
            .withHeader("Content-Type", "application/json")
            .withFault(Fault.EMPTY_RESPONSE)));

        //when
        //then
        then(remoteOfferClient.getOffers()).size().isEqualTo(0);
    }

    @Test
    void should_fail_with_malformed() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
            .willReturn(WireMock.aResponse()
            .withStatus(HttpStatus.SC_OK)
            .withHeader("Content-Tyoe", "application/json")
            .withFault(Fault.MALFORMED_RESPONSE_CHUNK)));

        //when
        //then
        then(remoteOfferClient.getOffers()).size().isEqualTo(0);
    }

    @Test
    void should_fail_with_random() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
            .willReturn(WireMock.aResponse()
            .withStatus(HttpStatus.SC_OK)
            .withHeader("Content-Tyoe", "application/json")
            .withFault(Fault.RANDOM_DATA_THEN_CLOSE)));

        //when
        //then
        then(remoteOfferClient.getOffers()).size().isEqualTo(0);
    }

    @Test
    void should_return_zero_job_offers_when_response_delay_is_1500_milis() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
            .willReturn(WireMock.aResponse()
            .withStatus(HttpStatus.SC_OK)
            .withHeader("Content-Type", "application/json")
            .withBody(bodyWithTwoOffersJson())
            .withFixedDelay(1500)));
        //when
        //then
        then(remoteOfferClient.getOffers()).size().isEqualTo(0);
    }

    @Test
    void should_return_response_not_found_status_exception_when_http_service_returning_not_found_status() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
            .willReturn(WireMock.aResponse()
            .withStatus(HttpStatus.SC_NOT_FOUND)
            .withHeader("Content-Type", "application/json")));
        //when
        //then
        BDDAssertions.thenThrownBy(()-> remoteOfferClient.getOffers()).hasMessage("404 NOT_FOUND");
    }

    @Test
    void should_return_response_unauthorized_status_exception_when_http_service_returning_unauthorized_status() {
        //given
        WireMock.stubFor(WireMock.get("/offers")
                .willReturn(WireMock.aResponse()
                        .withStatus(HttpStatus.SC_UNAUTHORIZED)
                        .withHeader("Content-Type", "application/json")));
        //when
        //then
        BDDAssertions.thenThrownBy(()-> remoteOfferClient.getOffers()).hasMessage("401 UNAUTHORIZED");
    }

    private String bodyWithTwoOffersJson() {
        return "[{\n" +
                "    \"title\": \"Software Engineer - Mobile (m/f/d)\",\n" +
                "    \"company\": \"Cybersource\",\n" +
                "    \"salary\": \"4k - 8k PLN\",\n" +
                "    \"url\": \"https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"title\": \"Junior DevOps Engineer\",\n" +
                "    \"company\": \"CDQ Poland\",\n" +
                "    \"salary\": \"8k - 14k PLN\",\n" +
                "    \"url\": \"https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd\"\n" +
                "  }]";
    }

    private String bodyWithOneOfferJson() {
        return "[{\n" +
                "    \"title\": \"Software Engineer - Mobile (m/f/d)\",\n" +
                "    \"company\": \"Cybersource\",\n" +
                "    \"salary\": \"4k - 8k PLN\",\n" +
                "    \"url\": \"https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn\"\n" +
                "  }]";
    }

    private String bodyWithZeroOfferJson() {
        return "[]";
    }

    private OfferDto cybersourceSoftwareEngineer() {
        return offerWithParameters("Software Engineer - Mobile (m/f/d)", "Cybersource", "4k - 8k PLN", "https://nofluffjobs.com/pl/job/software-engineer-mobile-m-f-d-cybersource-poznan-entavdpn");
    }

    private OfferDto cdqJuniorDevOpsEngineer() {
        return offerWithParameters("Junior DevOps Engineer", "CDQ Poland", "8k - 14k PLN","https://nofluffjobs.com/pl/job/junior-devops-engineer-cdq-poland-wroclaw-gnymtxqd");
    }
}
