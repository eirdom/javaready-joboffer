package com.junioroffers.offer.domain;

import com.junioroffers.JobOffersApplication;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.then;

@SpringBootTest(classes = JobOffersApplication.class)
@Testcontainers
@ActiveProfiles("container")
public class OfferServiceWithContainerTests implements SampleOffer, SampleOfferDto {

    @Container
    private static final MongoDBContainer DB_CONTAINER = new MongoDBContainer("mongo:4.0.4");

    static {
        DB_CONTAINER.start();
        System.setProperty("DB_PORT", String.valueOf(DB_CONTAINER.getFirstMappedPort()));
    }

    @Test
    void should_return_all_offers(@Autowired OfferRepository repository,
                                  @Autowired OfferService service){
        Offer cybersourceOffer = cybersourceOffer();
        Offer cdqPolandOffer = cdqPolandOffer();
        assertThat(repository.findAll()).hasSize(2);
        then(repository.findAll()).containsAll(Arrays.asList(cdqPolandOffer, cybersourceOffer));

        List<OfferDto> serviceOffer = service.findAllOffers();

        assertThat(serviceOffer).containsExactlyInAnyOrderElementsOf(
                Arrays.asList(cybersourceOfferDto(), cdqPolandOfferDto()));
    }

    @Test
    void should_return_offer_by_id(@Autowired OfferRepository repository,
                                   @Autowired OfferService service){
        OfferDto cybersourceOffer = cybersourceOfferDto();
        then(repository.findById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074")).isPresent();

        OfferDto serviceOffer = service.findOfferById("7b3e02b3-6b1a-4e75-bdad-cef5b279b074");
        assertThat(serviceOffer).isEqualTo(cybersourceOffer);
    }

    @Test
    void should_thrown_not_found_exception(@Autowired OfferRepository repository,
                                          @Autowired OfferService service){
        final String ID_NOT_EXIST= "IDNOTEXIST";
        then(repository.findById(ID_NOT_EXIST)).isNotPresent();

        Throwable throwable = catchThrowable(() -> service.findOfferById(ID_NOT_EXIST));

        AssertionsForClassTypes.assertThat(throwable)
                .isInstanceOf(OfferNotFoundException.class)
                .hasMessage("Offer with id " + ID_NOT_EXIST + " is not found");
    }
}
