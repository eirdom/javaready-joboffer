package com.junioroffers.offer;

import com.junioroffers.offer.domain.OfferRepository;
import com.junioroffers.offer.domain.OfferService;
import com.junioroffers.offer.domain.SampleOffer;
import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferControllerErrorHandler;
import com.junioroffers.offer.domain.exceptions.SampleOfferNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;
import java.util.List;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;


@WebMvcTest
@ContextConfiguration(classes = RestAssuredConfig.class)
class OfferControllerTest {

    final String EXISTING_ID = "24ee32b6-6b15-11eb-9439-0242ac130002";
    final String EXISTING_ID_2 = "7b3e02b3-6b1a-4e75-bdad-cef5b279b074";
    final String NO_EXISTING_ID = "javatest";

    @Test
    public void should_return_status_ok_when_get_for_offers(@Autowired WebApplicationContext context){
        given()
                .webAppContextSetup(context)
                .contentType("application/json")
                .when()
                .get("/offers")
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void should_return_status_ok_when_offer_with_given_id_exist(@Autowired WebApplicationContext context){
        given()
                .webAppContextSetup(context)
                .contentType("application/json")
                .when()
                .get("/offers/" + EXISTING_ID)
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void should_return_status_ok_when_offer_with_given_id_two(@Autowired WebApplicationContext context){
        given()
                .webAppContextSetup(context)
                .contentType("application/json")
                .when()
                .get("/offers/" + EXISTING_ID_2)
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void should_return_status_not_found_when_offer_with_given_id_no_exist(@Autowired WebApplicationContext context){
        given()
                .webAppContextSetup(context)
                .contentType("application/json")
                .when()
                .get("/offers" + NO_EXISTING_ID)
                .then().statusCode(HttpStatus.NOT_FOUND.value());

    }

}

@Configuration(proxyBeanMethods = false)
class RestAssuredConfig implements SampleOfferDto, SampleOfferNotFoundException {

    @Bean
    OfferService offerService(){
        OfferRepository offerRepository = Mockito.mock(OfferRepository.class);
        return new OfferService(offerRepository) {
            @Override
            public List<OfferDto> findAllOffers() {
                return Arrays.asList(cybersourceOfferDto(), cdqPolandOfferDto());
            }

            @Override
            public OfferDto findOfferById(String id) {
                if ("7b3e02b3-6b1a-4e75-bdad-cef5b279b074".equals(id)) {
                    return cybersourceOfferDto();
                } else if ("24ee32b6-6b15-11eb-9439-0242ac130002".equals(id)) {
                    return cdqPolandOfferDto();
                }
                throw sampleOfferNotFoundException(id);
            }
        };
    }

    @Bean
    OfferController offerController(OfferService offerService){
        return new OfferController(offerService);
    }

    @Bean
    OfferControllerErrorHandler offerControllerErrorHandler(){
        return new OfferControllerErrorHandler();
    }
}