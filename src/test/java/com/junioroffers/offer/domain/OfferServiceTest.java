package com.junioroffers.offer.domain;

import com.junioroffers.offer.domain.dto.OfferDto;
import com.junioroffers.offer.domain.dto.SampleOfferDto;
import com.junioroffers.offer.domain.exceptions.OfferNotFoundException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.BDDAssertions.*;
import static org.mockito.Mockito.when;

public class OfferServiceTest implements SampleOfferDto, SampleOffer{

    OfferRepository offerRepository = Mockito.mock(OfferRepository.class);
    private final String EXISTING_ID_ONE = "1L";
    private final String EXISTING_ID_TWO = "2L";
    private final String NO_EXISTING_ID = "100L";

    @Test
    public void should_return_two_expected_offers(){

//        when(offerRepository.findAll()).thenReturn(Arrays.asList(cdqPolandOffer(),cybersourceOffer()));
//        assertThat(offerRepository.findAll()).isEqualTo(Arrays.asList(cdqPolandOffer(), cybersourceOffer()));

//        OfferService offerService = new OfferService(offerRepository);
//
//        final List<OfferDto> expectedList = Arrays.asList(cybersourceOfferDto(),cdqPolandOfferDto());
//
//        final List<OfferDto> allOffer = offerService.findAllOffers();
//        then(allOffer).containsExactlyInAnyOrderElementsOf(expectedList);
    }

    @Test
    public void should_return_correct_offer_with_id_two(){
//        OfferService offerService = new OfferService();

//        final OfferDto offerById = offerService.findOfferById(EXISTING_ID_TWO);

//        then(offerById).isEqualTo(sample.findOfferById(EXISTING_ID_TWO));
    }

    @Test
    public void should_return_correct_offer_with_id_one(){
//        OfferService offerService = new OfferService();

//        final OfferDto offerById = offerService.findOfferById(EXISTING_ID_ONE);

//        then(offerById).isEqualTo(sample.findOfferById(EXISTING_ID_ONE));
    }

    @Test
    public void should_return_status_not_found_when_is_not_offer_with_given_id(){
//        OfferService offerService = new OfferService();

//        Throwable thrown = catchThrowable(() -> offerService.findOfferById(NO_EXISTING_ID));

//        assertThat(thrown)
//                .isInstanceOf(OfferNotFoundException.class)
//                .hasMessage("Offer with id 100 is not found");
    }
}