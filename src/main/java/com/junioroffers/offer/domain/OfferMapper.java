package com.junioroffers.offer.domain;

import com.junioroffers.offer.domain.dto.OfferDto;


public class OfferMapper {

    public static OfferDto mapToOfferDto(Offer offer){
        return OfferDto.builder()
                .id(offer.getId())
                .position(offer.getPosition())
                .companyName(offer.getCompanyName())
                .salary(offer.getSalary())
                .offerURL(offer.getOfferUrl())
                .build();
    }
}
